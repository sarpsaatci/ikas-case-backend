import { Document } from 'mongoose';
import * as jwt from 'jsonwebtoken';
import { User } from '../models/user';

export const getUserFromToken = async (authHeader: string): Promise<Document | unknown> => {
  if (authHeader) {
    const token = authHeader.replace('Bearer ', '');
    try {
      const { JWT_KEY } = process.env;
      if (JWT_KEY) {
        const data = await jwt.verify(token, JWT_KEY);
        if (data) {
          if (data.aud === 'user' && data.sub) {
            try {
              const user = await User.findOne({ email: data.sub });
              if (!user) {
                return null;
              }
              return user;
            } catch (error) {
              return null;
            }
          } else {
            return null;
          }
        }
      }
      return null;
    } catch (err) {
      return null;
    }
  }
  return null;
};

export const generateAuthToken = async (email: string): Promise<string> => new Promise(
  (resolve /* reject */) => {
    const token = jwt.sign({ sub: email }, process.env.JWT_KEY, {
      // algorithm: 'RS512',
      issuer: 'https://api.ikas.com',
      audience: 'user',
    });
    return resolve(token);
  },
);
