import * as mongoose from 'mongoose';
import { Connection } from 'mongoose';

export const dbConnection = (uri: string): Promise<void | Connection> => mongoose.connect(
  uri,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
).then((db: any) => {
  console.log('- DB connected');
  return db;
})
  .catch((err) => {
    console.log(err);
  });

mongoose.set('useCreateIndex', true);
mongoose.set('autoIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

export default dbConnection;
