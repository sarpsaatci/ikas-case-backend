/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { DirectiveArgs } from 'graphql-compose';
import { Document } from 'mongoose';
import { User as UserModel } from '../../models/user';
import { generateAuthToken } from '../../utils/auth';

export const registerResolver = (
  parent: any,
  args: DirectiveArgs,
): Promise<Document> => {
  const user = new UserModel({
    name: args.name,
    surname: args.surname,
    email: args.email,
    password: args.password,
  });
  return user.save();
};

export const loginResolver = (
  parent: any, args: DirectiveArgs,
): Promise<string> => new Promise((resolve, reject) => UserModel.findOne(
  { email: args.email },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (err: Error, user: any) => {
    if (err) return reject();
    if (user) {
      return user.comparePassword(
        args.password, user._doc,
        (error: Error, isMatch: boolean) => {
          if (error) {
            return reject();
          }
          if (isMatch && isMatch === true) {
            return generateAuthToken(user._doc.email)
              .then((token: string) => resolve(token))
              .catch((e) => reject(e));
          }
          return reject(new Error('invalid info'));
        },
      );
    }
    return reject(new Error('User not found'));
  },
));
