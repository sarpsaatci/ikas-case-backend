/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Document, Types } from 'mongoose';
import { DirectiveArgs } from 'graphql-compose';
import { Product as ProductModel } from '../../models/product';

export const addProductResolver = (
  parent: any,
  args: DirectiveArgs,
  context: Record<string, Document>,
): Promise<Document> => new Promise((resolve, reject) => {
  if (!context.user) {
    return reject(new Error('Not authorized'));
  }
  const product = new ProductModel({
    productName: args.productName,
    price: {
      currency: args.priceCurrency,
      amount: args.priceAmount,
    },
    barcode: args.barcode,
    quantity: args.quantity,
    userId: context.user._id,
  });
  return resolve(product.save());
});

export const deleteProductResolver = (
  parent: any,
  args: DirectiveArgs,
  context: Record<string, Document>,
): Promise<boolean> => new Promise(
  (resolve, reject) => {
    if (!context.user) {
      return reject(new Error('Not authorized'));
    }
    return ProductModel.findOneAndDelete(
      { _id: args.id, userId: context.user._id },
      null,
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      (/* err: Error, res: Document */) => resolve(true),
    );
  },
);

export const listProductsResolver = (
  parent: any,
  args: DirectiveArgs,
  context: Record<string, Document>,
): Promise<Document[]> => new Promise((resolve, reject) => {
  if (!context.user) {
    return reject(new Error('Not authorized'));
  }
  return ProductModel.find(
    { userId: Types.ObjectId(context.user._id) },
    (error: Error, products: Document[]) => resolve(products),
  );
});

export const productDetailsResolver = (
  parent: any,
  args: DirectiveArgs,
  context: Record<string, Document>,
): Promise<Document> => new Promise(
  (resolve, reject) => {
    if (!context.user) {
      return reject(new Error('Not authorized'));
    }
    return ProductModel.findOne(
      {
        _id: Types.ObjectId(args.id),
        userId: Types.ObjectId(context.user._id),
      }, (err: Error, product: Document) => resolve(product),
    );
  },
);
