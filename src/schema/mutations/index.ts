import { GraphQLObjectType } from 'graphql';
import userMutations from './user';
import productMutations from './product';

const mutations = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Root Mutation',
  fields: () => ({
    ...userMutations,
    ...productMutations,
  }),
});

export default mutations;
