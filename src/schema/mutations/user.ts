import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';
import { registerResolver } from '../resolvers/user';
import User from '../types/user';

const userMutations = {
  register: {
    type: User,
    description: 'Register a user',
    args: {
      name: { type: GraphQLNonNull(GraphQLString) },
      surname: { type: GraphQLNonNull(GraphQLString) },
      email: { type: GraphQLNonNull(GraphQLString) },
      password: { type: GraphQLNonNull(GraphQLString) },
    },
    resolve: registerResolver,
  },
};

export default userMutations;
