import { GraphQLMongoID } from 'graphql-compose-mongoose';
import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLFloat,
  GraphQLInt,
  GraphQLBoolean,
} from 'graphql';
import Product from '../types/product';
import { addProductResolver, deleteProductResolver } from '../resolvers/product';

const productMutations = {
  addProduct: {
    type: Product,
    description: 'Add a product',
    args: {
      productName: { type: GraphQLNonNull(GraphQLString) },
      priceCurrency: { type: GraphQLNonNull(GraphQLString) },
      priceAmount: { type: GraphQLNonNull(GraphQLFloat) },
      barcode: { type: GraphQLNonNull(GraphQLString) },
      quantity: { type: GraphQLNonNull(GraphQLInt) },
    },
    resolve: addProductResolver,
  },
  deleteProduct: {
    type: GraphQLBoolean,
    description: 'Delete a product',
    args: {
      id: { type: GraphQLNonNull(GraphQLMongoID) },
    },
    resolve: deleteProductResolver,
  },
};

export default productMutations;
