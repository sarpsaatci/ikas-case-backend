import { GraphQLObjectType } from 'graphql';
import { Document } from 'mongoose';
import userQueries from './user';
import productQueries from './product';

const queries = new GraphQLObjectType({
  name: 'Query',
  description: 'Root Query',
  fields: () => ({
    ...userQueries,
    ...productQueries,
  }),
});

export default queries;
