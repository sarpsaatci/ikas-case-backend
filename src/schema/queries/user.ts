import {
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';
import { loginResolver } from '../resolvers/user';

const userQueries = {
  login: {
    type: GraphQLString,
    description: 'Authenticate user, returns access token',
    args: {
      email: { type: GraphQLNonNull(GraphQLString) },
      password: { type: GraphQLNonNull(GraphQLString) },
    },
    resolve: loginResolver,
  },
};

export default userQueries;
