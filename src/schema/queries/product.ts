import { GraphQLMongoID } from 'graphql-compose-mongoose';
import { GraphQLList } from 'graphql';
import Product from '../types/product';
import { listProductsResolver, productDetailsResolver } from '../resolvers/product';

const productQueries = {
  listProducts: {
    type: new GraphQLList(Product),
    description: 'List of Products of a user',
    resolve: listProductsResolver,
  },
  product: {
    type: Product,
    description: 'Get details of specified product',
    args: {
      id: { type: GraphQLMongoID },
    },
    resolve: productDetailsResolver,
  },
};

export default productQueries;
