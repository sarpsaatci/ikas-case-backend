import { GraphQLMongoID } from 'graphql-compose-mongoose';

import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

const User = new GraphQLObjectType({
  name: 'User',
  description: 'Represents user',
  fields: () => ({
    _id: { type: GraphQLNonNull(GraphQLMongoID) },
    name: { type: GraphQLNonNull(GraphQLString) },
    surname: { type: GraphQLNonNull(GraphQLString) },
    email: { type: GraphQLNonNull(GraphQLString) },
    password: { type: GraphQLNonNull(GraphQLString) },
  }),
});

export default User;
