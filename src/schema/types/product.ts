import {
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import { GraphQLMongoID } from 'graphql-compose-mongoose';
import User from './user';
import { User as UserModel } from '../../models/user';

export const Price = new GraphQLObjectType({
  name: 'Price',
  description: 'Represents price of a product',
  fields: () => ({
    currency: { type: GraphQLNonNull(GraphQLString) },
    amount: { type: GraphQLNonNull(GraphQLFloat) },
  }),
});

const Product = new GraphQLObjectType({
  name: 'Product',
  description: 'Represents product of a user',
  fields: () => ({
    _id: { type: GraphQLString },
    productName: { type: GraphQLNonNull(GraphQLString) },
    price: { type: Price },
    barcode: { type: GraphQLNonNull(GraphQLString) },
    quantity: { type: GraphQLNonNull(GraphQLInt) },
    userId: { type: GraphQLMongoID },
    user: {
      type: User,
      resolve: ({ userId }: {userId: string}) => UserModel.findOne(
        { _id: userId }, (err: Error, userRes: Document) => userRes,
      ),
    },
  }),
});

export default Product;
