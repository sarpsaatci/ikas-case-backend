import * as dotenv from 'dotenv';
import * as express from 'express';
import { ApolloServer } from 'apollo-server-express';
import * as mongoose from 'mongoose';

import { dbConnection } from './utils/db';
import schema from './schema';
import { getUserFromToken } from './utils/auth';

import buildSchema from './scripts/buildSchema';

dotenv.config();

const app = express();

const uri: string = process.env.MONGODB_URI || '';

const server = new ApolloServer({
  schema,
  playground: process.env.NODE_ENV === 'development',
  introspection: true,
  tracing: true,
  context: async ({ req }) => {
    const authHeader = req.headers.authorization || '';
    const user = await getUserFromToken(authHeader);
    if (user) {
      return { user };
    }
    return null;
  },
});

server.applyMiddleware({
  app,
  path: '/',
  cors: true,
  onHealthCheck: () => new Promise((resolve, reject) => {
    if (mongoose.connection.readyState > 0) {
      resolve(true);
    } else {
      reject();
    }
  }),
});

app.listen({ port: process.env.PORT || 3000 }, () => {
  dbConnection(uri);
  console.log(`- Server listening on port ${process.env.PORT || 3000}`);
  buildSchema();
});
