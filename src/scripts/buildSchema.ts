import * as fs from 'fs';
import * as path from 'path';
import { graphql } from 'graphql';
import { getIntrospectionQuery, printSchema } from 'graphql/utilities';

import Schema from '../schema';

async function buildSchema() {
  fs.writeFileSync(
    path.join(__dirname, '../data/schema.graphql.json'),
    JSON.stringify(await graphql(Schema, getIntrospectionQuery()), null, 2),
  );

  fs.writeFileSync(
    path.join(__dirname, '../data/schema.graphql.txt'),
    printSchema(Schema),
  );
}

export default async function run(): Promise<void> {
  await buildSchema();
  console.log('- Schema build complete!');
}
