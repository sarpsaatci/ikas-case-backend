/* eslint-disable @typescript-eslint/no-explicit-any */
import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';
import * as bcrypt from 'bcrypt';

export const UserSchema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
    },
    surname: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    collection: 'users',
  },
);

UserSchema.index({ createdAt: 1, updatedAt: 1 });

UserSchema.pre('save', function preSave(this: any, next) {
  const SALT_WORK_FACTOR = 10;
  // eslint-disable-next-line @typescript-eslint/no-this-alias
  const user: Record<string, any> = this;
  if (user && !user.isModified('password')) return next();
  return bcrypt.genSalt(SALT_WORK_FACTOR, (err: Error, salt: string) => {
    if (err) {
      console.log(err);
      return next(err);
    }

    // hash the password along with our new salt
    return bcrypt.hash(user.password, salt, (error: Error, hash: string) => {
      if (error) return next(error);

      // override the cleartext password with the hashed one
      user.password = hash;
      return next();
    });
  });
});

UserSchema.methods.comparePassword = (candidatePassword, user, cb) => {
  bcrypt.compare(candidatePassword, user.password, (err, isMatch: boolean) => {
    if (err) return cb(err);
    return cb(null, isMatch);
  });
};

export const User = mongoose.model('User', UserSchema);
