import * as mongoose from 'mongoose';
import { Schema } from 'mongoose';

export const ProductSchema = new Schema(
  {
    productName: {
      type: String,
      trim: true,
      required: true,
    },
    price: {
      type: Object,
      required: true,
    },
    barcode: {
      type: String,
      trim: true,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  },
  {
    collection: 'products',
  },
);

ProductSchema.index({ createdAt: 1, updatedAt: 1 });

export const Product = mongoose.model('Product', ProductSchema);
