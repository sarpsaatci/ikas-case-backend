// import databaseConn, { closeDatabaseConn } from '../../src/helpers/database';
// import AuthManager from '../../src/api/auth/manager';
// import { UserInput } from '../../src/api/auth/models';
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const { registerResolver, loginResolver } = require('../../src/schema/resolvers/user');

dotenv.config();

const User = require('../../src/models/user');

const { dbConnection } = require('../../src/utils/db');
const jwt = require('jsonwebtoken');

const userInput = {
  name: 'test name',
  surname: 'test surname',
  email: 'test@test.com',
  password: 'testPwd',
};

const uri = process.env.MONGODB_URI || '';

beforeAll(async () => {
  await dbConnection(uri);
});
afterAll(async () => {
  mongoose.connection.on('open', async () => {
    await mongoose.connection.db.dropDatabase();
  });
  // await connection.dropDatabase();
});

let accessToken = '';

describe('user register/login tests', () => {
  it('User Registered', async () => {
    const user = await registerResolver(null, userInput);
    expect(user.email).toBe(userInput.email);
  });

  it('User Logged In', async () => {
    accessToken = await loginResolver(null, {
      email: userInput.email,
      password: userInput.password,
    });
    const decodedToken = await jwt.verify(accessToken, process.env.JWT_KEY);
    expect(decodedToken.sub).toBe(userInput.email);
  });
});
